import {AuthService} from '../../../auth/auth.service';
import {PostsService} from '../../../shared/services/posts.service';
import {Router} from '@angular/router';

export declare interface IPost {
    title: string;
    body: string;
    author: string;
    summary: string;
    tags?: Array<number>;
    picture: Array<any>;
    comments?: Array<any>;
}


import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
    selector: 'app-admin-panel',
    templateUrl: './admin-panel.component.html',
    styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {

    public newPost: IPost;
    public options: any;
    public tags: Array<any>;
    public tagsControl: FormControl;

    constructor(public auth: AuthService, public postsService: PostsService, public router: Router) {
    }

    async ngOnInit() {
        this.newPost = {
            title: '',
            author: null,
            body: '',
            summary: '',
            picture: null,
            tags: []
        };
        this.tagsControl = new FormControl();
        const object = await this.postsService.getTags().toPromise();
        this.tags = [...object.data['tags']];
        this.options = {
            showPreviewPanel: true,   // Show preview panel, Default is true
            showBorder: true,       // Show editor component's border. Default is true
            usingFontAwesome5: true,    // Using font awesome with version 5, Default is false
            scrollPastEnd: 0,       // The option for ace editor. Default is 0
            resizable: false           // Allow resize the editor
        };
    }

    async submitPost() {
        this.newPost.tags = [...this.tagsControl.value];
        const user = await this.auth.getUser$().toPromise();
        if (user && user.author) {
            this.newPost.author = user.author;
        } else {
            this.newPost.author = '5e84ed36f5860827e5b61832';
        }
        try {
            const object = await this.postsService.submitPost(this.newPost).toPromise();
            const post = object.data['addPost'];
            return await this.router.navigate([`/posts/${post.id}`]);
        } catch (e) {
            console.log(e);
        }

    }
}
