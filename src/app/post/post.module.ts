import {NgModule, SecurityContext} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PostComponent} from './components/post/post.component';
import {PostRoutingModule} from './post-routing.module';
import {PostsService} from '../shared/services/posts.service';
import {LoadingService} from '../shared/services/loading.service';
import {MatCardModule} from '@angular/material/card';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {MarkdownModule, MarkdownService, MarkedOptions} from 'ngx-markdown';
import {HttpClient} from '@angular/common/http';


@NgModule({
    declarations: [
        PostComponent
    ],
    imports: [
        CommonModule,
        PostRoutingModule,
        MatCardModule,
        TranslateModule,
        MarkdownModule.forRoot({
            loader: HttpClient,
            markedOptions: { provide: MarkedOptions },
            sanitize: SecurityContext.HTML, // default value
        }),
    ],
    providers: [
        PostsService,
        LoadingService,
        MarkdownService
    ]
})
export class PostModule {
    constructor(private translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/post.${culture}.json`).then((translations) => {
                this.translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
